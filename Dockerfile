FROM python:3.7-alpine

WORKDIR /build

# Install OS requirements
COPY ./docker/requirements_os.txt /build
RUN apk add --update $(grep -vE "^\s*#" 'requirements_os.txt'  | tr "\n" " ") && \
    rm -rf /var/cache/apk

# Install python requirements
RUN pip install --no-cache-dir --upgrade pip==18.0 pipenv==2018.7.1
COPY ./Pipfile* /build/
RUN pipenv install --system --deploy --ignore-pipfile

RUN mkdir -p /exec

COPY ./docker/bootstrap.sh /exec/

# This is useful if we want to run the image outside docker-compose (kubernetes for example)
VOLUME '/static'

WORKDIR /code

COPY ./mastermind /code

CMD ["/bin/sh", "/exec/bootstrap.sh"]

EXPOSE 8000
