#!/usr/bin/env python
"""
Wrapper around manage.py to compute full coverage.

It allows coverage to detect import-time code (
    such as Django models, metaclasses, etc.).
"""

if __name__ == "__main__":
    from django.core.management import execute_from_command_line
    from coverage import Coverage

    cov = Coverage(omit=['*/tests/*', '*/admin.py'])
    cov.set_option('report:show_missing', True)
    cov.erase()
    cov.start()

    execute_from_command_line(['manage.py', 'test', '-v2'])

    cov.stop()
    cov.save()
    cov.report()
