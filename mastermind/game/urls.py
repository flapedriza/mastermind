from django.urls import path, include
from rest_framework import routers

from game.views import UserRegistrationView, GamesViewSet, AttemptAPIView

router = routers.DefaultRouter()
router.register(r'games', GamesViewSet, base_name='games')

urlpatterns = [
    path('register/', UserRegistrationView.as_view(), name='api-register'),
    path('attempt/', AttemptAPIView.as_view(), name='api-make-attempt'),
    path('', include(router.urls))
]
