# Create your views here.
from django.shortcuts import get_object_or_404
from rest_framework import status, generics, permissions, viewsets
from rest_framework.authtoken.models import Token
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from game.models import Game, Attempt
from game.serializers import UserSerializer, GameSerializer, GameDetailSerializer, AttemptSerializer


class UserRegistrationView(generics.CreateAPIView):
    """
    API view to register a user by providing a username and a password,
    an authentication token is returned in response
    """
    serializer_class = UserSerializer
    permission_classes = [permissions.AllowAny]

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            if user:
                token = Token.objects.create(user=user)
                json = serializer.data
                json['token'] = token.key
                return Response(json, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class GamesViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API viewset to retrieve all games for a certain user, create a new game or
    activate a current game.

    retrieve:
    Returns the given game and the info about all attempts to that game

    list:
    Returns a list of all user's games and their number of attempts
    """
    permission_classes = [IsAuthenticated]

    def get_serializer_class(self):
        if self.action in ['retrieve', 'active']:
            return GameDetailSerializer
        return GameSerializer

    def get_queryset(self):
        return self.request.user.games.all()

    @action(detail=False, methods=['POST'])
    def new(self, request):
        """
        Endpoint to create a new game for the current user, the newly created
        game will be marked as active.
        """
        user = request.user
        game = Game.objects.create(user=user)

        serializer = self.get_serializer(game)

        return Response(serializer.data, status.HTTP_201_CREATED)

    @action(detail=True, methods=['POST'])
    def activate(self, request, pk=None):
        """
        Endpoint to activate a previously created non-completed game, the game
        id must be provided in the URL
        """
        queryset = request.user.games
        game = get_object_or_404(queryset, pk=pk)
        if game.completed:
            return Response("Can't activate a completed game", status.HTTP_409_CONFLICT)
        game.active = True
        game.save()

        serializer = self.get_serializer(game)

        return Response(serializer.data, status.HTTP_200_OK)

    @action(detail=False, methods=['GET'])
    def active(self, request):
        """
        Endpoint to get the information about the currently active game
        """
        game = get_object_or_404(self.get_queryset(), active=True)
        serializer = self.get_serializer(game)

        return Response(serializer.data, status.HTTP_200_OK)


class AttemptAPIView(generics.CreateAPIView):
    """
    API view to make a new attempt to crack the current game, a guess must be
    provided.
    The attempt result info about whether the attempt was successful and a
    boolean stating if the current game has finished will be provided in
    response.
    """
    serializer_class = AttemptSerializer

    def post(self, request, format=None):
        user = request.user

        try:
            game = user.games.get(active=True)
        except Game.DoesNotExist:
            return Response(
                'The user does not have any active game',
                status.HTTP_404_NOT_FOUND
            )
        except Game.MultipleObjectsReturned:  # pragma: no cover (Can't force because of model save)
            return Response(
                'Somehow, the user has multiple active games',
                status.HTTP_500_INTERNAL_SERVER_ERROR
            )

        serializer = self.serializer_class(data=request.POST)

        if serializer.is_valid():
            response = {}
            guess = serializer.validated_data['guess']

            print(game)

            attempt = Attempt.objects.create(
                game=game,
                guess=guess
            )

            resp_seriaizer = self.serializer_class(attempt)
            game.refresh_from_db()
            response['game_completed'] = game.completed
            response.update(resp_seriaizer.data)
            return Response(response, status.HTTP_201_CREATED)
        return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)
