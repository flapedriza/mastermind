BLUE = 'Blue'
RED = 'Red'
GREEN = 'Green'
YELLOW = 'Yellow'
WHITE = 'White'
BLACK = 'Black'
NONE = 'None'

COLOR_CHOICES = (
    (BLUE, 'Blue'),
    (RED, 'Red'),
    (GREEN, 'Green'),
    (YELLOW, 'Yellow'),
    (WHITE, 'White'),
    (BLACK, 'Black'),

)

GUESS_COLOR_CHOICES = (
    (NONE, 'None'),
    (BLACK, 'Black'),
    (WHITE, 'White')
)
