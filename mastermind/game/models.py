from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.postgres.fields import ArrayField
from django.db import models

from common.models import TimestampedModel
from game.choices import COLOR_CHOICES, GUESS_COLOR_CHOICES
from game.utils import generate_secret_code, generate_result


class Game(TimestampedModel):
    """
    This model represents a game within the database, the game is related to
    user to be able to obtain all games for a certain user, a game can be either
    active or not active, but a user can only have an active game at the same
    time. Every time a new game is created, a secret code is randomly generated
    and when a game is activated, all other active games for that user get
    deactivated. This logic is implemented at model-level to avoid repeating
    code if the model is used outside API views
    """

    user = models.ForeignKey(
        User,
        null=False,
        on_delete=models.CASCADE,
        related_name='games'
    )

    active = models.BooleanField(default=True)
    completed = models.BooleanField(default=False)

    secret_code = ArrayField(
        models.CharField(max_length=20, choices=COLOR_CHOICES),
        size=settings.SECRET_CODE_LENGTH,
        default=generate_secret_code,
        editable=False
    )

    def save(self, *args, **kwargs):
        if self.active:
            # If a new game is being created or the user activates an unactive
            # game all other games for this user should be marked as unactive
            (self.__class__.objects
             .filter(user=self.user)
             .update(active=False))
        super(Game, self).save(*args, **kwargs)

    class Meta:
        ordering = ['-active', 'user']


class Attempt(TimestampedModel):
    """
    This model represents an attempt to break the code for a certain game, each
    time a new attempt is created all its extra data (number, result, success)
    is computed on the fly to avoid repeating this logic if the model is used
    outside API views. Also, when an attempt is successful or the maximum number
    of attempts is reached, the game is marked as completed.
    """
    game = models.ForeignKey(
        Game,
        null=False,
        on_delete=models.CASCADE,
        related_name='attempts'
    )

    attempt_number = models.PositiveSmallIntegerField()

    guess = ArrayField(
        models.CharField(max_length=20, choices=COLOR_CHOICES),
        size=settings.SECRET_CODE_LENGTH,
        blank=False,
        null=False
    )

    result = ArrayField(
        models.CharField(max_length=20, choices=GUESS_COLOR_CHOICES),
        size=settings.SECRET_CODE_LENGTH,
        blank=False,
        null=False,
    )

    success = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        if not self.pk:
            # If it's a new attempt calculate everything from guess
            result, success = generate_result(self.guess, self.game.secret_code)
            self.result = result
            self.success = success
            attempt_num = self.game.attempts.count() + 1
            self.attempt_number = attempt_num
            # If the guess was correct or we reached the maximum number of
            # attempts, then this game is finished
            if success or attempt_num >= settings.MAX_ATTEMPTS:
                self.game.completed = True
                self.game.active = False
                self.game.save()
        super(Attempt, self).save(*args, **kwargs)

    class Meta:
        ordering = ['game', 'attempt_number']
        unique_together = ('game', 'attempt_number')
