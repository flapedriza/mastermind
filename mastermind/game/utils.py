import random

from django.conf import settings

from game.choices import COLOR_CHOICES, NONE, BLACK, WHITE


def generate_secret_code():
    length = settings.SECRET_CODE_LENGTH
    colors = [c[1] for c in COLOR_CHOICES]
    return [random.choice(colors) for _ in range(length)]


def generate_result(guess, code):
    length = settings.SECRET_CODE_LENGTH
    white, black = 0, 0
    result = [NONE] * length

    for g, c in zip(guess, code):
        if g == c:
            result[black] = BLACK
            black += 1

    success = black == length

    if not success:
        tmp = code[:]
        for g in guess:
            if g in tmp:
                tmp.remove(g)
                if result[white] != BLACK:
                    result[white] = WHITE
                white += 1

    return result, success
