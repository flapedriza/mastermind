from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.auth.password_validation import validate_password
from django.contrib.auth import validators
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from game.choices import COLOR_CHOICES, GUESS_COLOR_CHOICES
from game.models import Game, Attempt


class UserSerializer(serializers.ModelSerializer):
    username = serializers.CharField(
        validators=[
            UniqueValidator(queryset=User.objects.all()),
            validators.UnicodeUsernameValidator()
        ],
        required=True,
        help_text='User username (must be unique)'
    )

    password = serializers.CharField(
        required=True,
        write_only=True,
        validators=[validate_password],
        help_text='User password'
    )

    def create(self, validated_data):
        username = validated_data['username']
        password = validated_data['password']

        return User.objects.create_user(username=username, password=password)

    class Meta:
        model = User
        fields = ('username', 'password')


class AttemptSerializer(serializers.ModelSerializer):
    attempt_number = serializers.IntegerField(read_only=True)

    guess = serializers.ListField(
        child=serializers.ChoiceField(choices=COLOR_CHOICES),
        min_length=settings.SECRET_CODE_LENGTH,
        max_length=settings.SECRET_CODE_LENGTH,
        help_text='User guess in the form of a color list'
    )

    result = serializers.ListField(
        child=serializers.ChoiceField(choices=GUESS_COLOR_CHOICES),
        read_only=True
    )

    success = serializers.BooleanField(read_only=True)

    class Meta:
        model = Attempt
        fields = ('attempt_number', 'guess', 'result', 'success')


class GameSerializer(serializers.ModelSerializer):
    attempts = serializers.SerializerMethodField()

    def get_attempts(self, obj):
        return obj.attempts.count()

    class Meta:
        model = Game
        fields = ('id', 'active', 'completed', 'attempts')
        read_only_fields = ('id', 'active', 'completed', 'attempts')


class GameDetailSerializer(GameSerializer):
    attempts = AttemptSerializer(many=True, read_only=True)

    class Meta(GameSerializer.Meta):
        pass
