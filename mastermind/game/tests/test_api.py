from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.test import override_settings
from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase

from game.choices import YELLOW, GREEN, RED, BLUE, BLACK, NONE, WHITE
from game.models import Game, Attempt


class UserRegistrationTest(APITestCase):
    def setUp(self):
        self.existing_user = User.objects.create(username='existing')

    def test_try_create_existing_user(self):
        """
        Try creating a user that already exists
        """

        url = reverse('api-register')
        payload = {
            'username': 'existing',
            'password': 'pss123456'
        }

        response = self.client.post(url, payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('This field must be unique.', response.data['username'])

    def test_no_user_provided(self):
        """
        Try sending a blank or null username
        """
        url = reverse('api-register')
        payload = {
            'username': '',
            'password': 'pss123456'
        }
        response = self.client.post(url, payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('This field may not be blank.', response.data['username'])

        payload = {
            'password': 'pss123456'
        }
        response = self.client.post(url, payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('This field is required.', response.data['username'])

    def test_invalid_username(self):
        url = reverse('api-register')
        payload = {
            'username': '%%%%',
            'password': 'pss123456'
        }
        response = self.client.post(url, payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn(
            'Enter a valid username. This value may contain only letters, '
            'numbers, and @/./+/-/_ characters.',
            response.data['username']
        )

    def test_bad_passwords(self):
        url = reverse('api-register')
        payload = {
            'username': 'user',
            'password': '1234567'
        }
        response = self.client.post(url, payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn(
            'This password is too short. It must contain at least '
            '8 characters.',
            response.data['password']
        )

        payload = {
            'username': 'user',
            'password': 'admin123'
        }
        response = self.client.post(url, payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn(
            'This password is too common.',
            response.data['password']
        )

    def test_user_created_successfully(self):
        url = reverse('api-register')
        payload = {
            'username': 'user',
            'password': 'pss1234567'
        }
        response = self.client.post(url, payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        self.assertTrue(User.objects.filter(username='user').exists())
        self.assertIsNotNone(authenticate(**payload))
        token = Token.objects.get(user__username='user')
        self.assertEqual(response.data['token'], token.key)


@override_settings(SECRET_CODE_LENGTH=4)
class GamesAPITest(APITestCase):
    def setUp(self):
        self.user1 = User.objects.create(username='user1')
        self.user2 = User.objects.create(username='user2')
        self.token1 = Token.objects.create(user=self.user1)
        self.token2 = Token.objects.create(user=self.user2)

        self.game1_user1 = Game.objects.create(  # This game is not active
            user=self.user1,
            secret_code=[BLUE, RED, GREEN, YELLOW],
            active=False
        )
        self.game2_user1 = Game.objects.create(  # This game is not active
            user=self.user1,
            secret_code=[BLUE, BLUE, YELLOW, BLACK],
            active=False
        )
        self.game3_user1 = Game.objects.create(  # This game is active
            user=self.user1,
            secret_code=[BLUE, BLUE, YELLOW, BLACK]
        )

        self.game1_user2 = Game.objects.create(
            user=self.user2,
            secret_code=[BLUE, RED, GREEN, YELLOW]
        )

        Attempt.objects.create(
            game=self.game2_user1,
            guess=[BLUE, BLUE, BLUE, BLUE]
        )

        Attempt.objects.create(  # After this, game 2 is finished and has 2 attempts
            game=self.game2_user1,
            guess=[BLUE, BLUE, YELLOW, BLACK],
        )

    def test_get_games(self):

        list_url = reverse('games-list')
        detail_url = reverse('games-detail', args=(1,))
        active_url = reverse('games-active')

        # Check that user must be authenticated

        response = self.client.get(list_url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.get(detail_url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.get(active_url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token1.key)

        response = self.client.get(list_url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(len(response.data), 3)

        # Try retrieving every game for user 1 and check data is correct
        for i in range(1, 4):
            game = getattr(self, 'game{}_user1'.format(i))
            url = reverse('games-detail', args=(game.pk,))
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)

            self.assertEqual(response.data['id'], game.pk)
            self.assertEqual(response.data['active'], game.active)
            self.assertEqual(response.data['completed'], game.completed)
            self.assertEqual(len(response.data['attempts']), game.attempts.all().count())

            if game.attempts.all().exists():
                for j, a in enumerate(game.attempts.all()):
                    self.assertEqual(
                        response.data['attempts'][j]['attempt_number'],
                        a.attempt_number
                    )
                    self.assertEqual(
                        response.data['attempts'][j]['guess'],
                        a.guess
                    )
                    self.assertEqual(
                        response.data['attempts'][j]['result'],
                        a.result
                    )
                    self.assertEqual(
                        response.data['attempts'][j]['success'],
                        a.success
                    )

        # Obtain the currently active game for user 1 (should be game 3)
        response = self.client.get(active_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], self.game3_user1.pk)
        self.assertEqual(response.data['active'], self.game3_user1.active)
        self.assertEqual(response.data['completed'], self.game3_user1.completed)

        # Try obtaining the game from user 2 using user 1 credentials (should return 404)
        url = reverse('games-detail', args=(self.game1_user2.pk,))

        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # Obtain the game we could not obtain before by authenticating with user 2
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token2.key)

        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], self.game1_user2.pk)
        self.assertEqual(response.data['active'], self.game1_user2.active)
        self.assertEqual(response.data['completed'], self.game1_user2.completed)

        # Obtain the currently active game for user 2 (the only one it has)
        response = self.client.get(active_url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], self.game1_user2.pk)
        self.assertEqual(response.data['active'], self.game1_user2.active)
        self.assertEqual(response.data['completed'], self.game1_user2.completed)

    def test_create_new_game(self):
        url = reverse('games-new')

        # This endpoint must be authenticated
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        games_num = Game.objects.filter(user=self.user1).count()

        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token1.key)

        response = self.client.post(url)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['active'], True)
        self.assertEqual(response.data['completed'], False)
        game_id = response.data['id']
        self.assertEqual(Game.objects.filter(user=self.user1).count(), games_num + 1)

        try:
            game = Game.objects.get(user=self.user1, active=True)
        except Game.DoesNotExist:
            self.fail('No game created')
        except Game.MultipleObjectsReturned:
            self.fail('Multiple active games after creating a new one')

        self.assertEqual(game.pk, game_id)

    def test_activate_game(self):
        url = reverse('games-activate', args=(1,))

        # This must be authenticated
        response = self.client.post(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token1.key)

        # Try activating a non-existent game
        url = reverse('games-activate', args=(999,))
        response = self.client.post(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # Try activating a game from another user
        url = reverse('games-activate', args=(self.game1_user2.pk,))
        response = self.client.post(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # Try activating a complete game
        url = reverse('games-activate', args=(self.game2_user1.pk,))
        response = self.client.post(url)
        self.assertEqual(response.status_code, status.HTTP_409_CONFLICT)
        self.assertEqual(response.data, "Can't activate a completed game")

        # Now activate an unactive game
        url = reverse('games-activate', args=(self.game1_user1.pk,))
        response = self.client.post(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(response.data['id'], self.game1_user1.pk)
        self.assertEqual(response.data['active'], True)
        self.assertEqual(response.data['completed'], False)

        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token2.key)

        # Activate game from user 2 even if it's active
        url = reverse('games-activate', args=(self.game1_user2.pk,))
        response = self.client.post(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(response.data['id'], self.game1_user2.pk)
        self.assertEqual(response.data['active'], True)
        self.assertEqual(response.data['completed'], False)


@override_settings(SECRET_CODE_LENGTH=4)
class AttemptAPITest(APITestCase):
    def setUp(self):
        self.user = User.objects.create(username='username')

        self.user_no_games = User.objects.create(username='user_no_games')
        self.user_no_active_games = User.objects.create(username='user_no_active_games')

        self.token = Token.objects.create(user=self.user)
        self.token_no_games = Token.objects.create(user=self.user_no_games)
        self.token_no_active_games = Token.objects.create(user=self.user_no_active_games)

        Game.objects.create(
            user=self.user_no_active_games,
            active=False
        )

        self.good_game_wp = Game.objects.create(
            user=self.user,
            secret_code=[BLUE, RED, GREEN, YELLOW],
        )

        Game.objects.create(  # Unactive game to make sure actions are executed against active game
            user=self.user,
            secret_code=[RED, RED, RED, RED],
            active=False
        )

        self.url = reverse('api-make-attempt')

    def test_guess_no_games(self):
        # This must be authenticated
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token_no_games.key)

        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(response.data, 'The user does not have any active game')

    def test_guess_no_active_games(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token_no_active_games.key)

        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(response.data, 'The user does not have any active game')

    def test_guess_validation_errors(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)

        # Check that a guess must be provided
        payload = {}

        response = self.client.post(self.url, data=payload)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('Ensure this field has at least 4 elements.', response.data['guess'])

        # Check that the guess must have 4 or more elements
        payload = {
            'guess': [BLUE, BLUE, BLUE]
        }

        response = self.client.post(self.url, data=payload)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('Ensure this field has at least 4 elements.', response.data['guess'])

        # Check that the guess must have 4 or less elements
        payload = {
            'guess': [BLUE, BLUE, BLUE, BLUE, BLUE]
        }

        response = self.client.post(self.url, data=payload)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('Ensure this field has no more than 4 elements.', response.data['guess'])

        # Check that guess values must be color choices
        payload = {
            'guess': [12, 'RED', BLUE]
        }
        response = self.client.post(self.url, data=payload)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('"12" is not a valid choice.', response.data['guess'][0])
        self.assertIn('"RED" is not a valid choice.', response.data['guess'][1])

    def test_good_guesses(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)

        payload = {
            'guess': [BLUE, RED, GREEN, BLUE]
        }
        response = self.client.post(self.url, data=payload)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['attempt_number'], 1)
        self.assertEqual(response.data['guess'], payload['guess'])
        self.assertEqual(response.data['result'], [BLACK, BLACK, BLACK, NONE])
        self.assertFalse(response.data['success'])
        self.assertFalse(response.data['game_completed'])

        self.good_game_wp.refresh_from_db()
        self.assertEqual(self.good_game_wp.attempts.count(), 1)

        payload = {
            'guess': [BLACK, RED, GREEN, BLUE]
        }
        response = self.client.post(self.url, data=payload)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['attempt_number'], 2)
        self.assertEqual(response.data['guess'], payload['guess'])
        self.assertEqual(response.data['result'], [BLACK, BLACK, WHITE, NONE])
        self.assertFalse(response.data['success'])
        self.assertFalse(response.data['game_completed'])

        self.good_game_wp.refresh_from_db()
        self.assertEqual(self.good_game_wp.attempts.count(), 2)

        payload = {
            'guess': [YELLOW, GREEN, RED, BLUE]
        }
        response = self.client.post(self.url, data=payload)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['attempt_number'], 3)
        self.assertEqual(response.data['guess'], payload['guess'])
        self.assertEqual(response.data['result'], [WHITE, WHITE, WHITE, WHITE])
        self.assertFalse(response.data['success'])
        self.assertFalse(response.data['game_completed'])

        self.good_game_wp.refresh_from_db()
        self.assertEqual(self.good_game_wp.attempts.count(), 3)

        payload = {
            'guess': [BLUE, RED, GREEN, YELLOW]
        }
        response = self.client.post(self.url, data=payload)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['attempt_number'], 4)
        self.assertEqual(response.data['guess'], payload['guess'])
        self.assertEqual(response.data['result'], [BLACK, BLACK, BLACK, BLACK])
        self.assertTrue(response.data['success'])
        self.assertTrue(response.data['game_completed'])

        self.good_game_wp.refresh_from_db()
        self.assertEqual(self.good_game_wp.attempts.count(), 4)

        # As the game is now complete due to success, now the user has no active games
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(response.data, 'The user does not have any active game')

    @override_settings(MAX_ATTEMPTS=5)
    def test_finish_attempts(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)

        payload = {
            'guess': [RED, RED, RED, RED]
        }

        for _ in range(4):
            self.client.post(self.url, data=payload)

        response = self.client.post(self.url, data=payload)
        self.assertFalse(response.data['success'])
        self.assertTrue(response.data['game_completed'])

        # As the game is now complete due to failing too much, now the user has no active games
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(response.data, 'The user does not have any active game')
