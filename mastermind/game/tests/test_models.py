from django.conf import settings
from django.contrib.auth.models import User
from django.test import TestCase, override_settings

from game.choices import COLOR_CHOICES, GREEN, RED, BLUE, YELLOW, BLACK, WHITE, NONE
from game.models import Game, Attempt


@override_settings(SECRET_CODE_LENGTH=4)
class GameModelTest(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='test_user')
        self.user2 = User.objects.create(username='test_user2')

    def test_create_single_game(self):
        g = Game.objects.create(
            user=self.user
        )

        # Check that a new game is always the active one and is not finalized
        self.assertTrue(g.active)
        self.assertFalse(g.completed)

        # Check that when creating a new game a random secret code is generated
        self.assertEqual(len(g.secret_code), settings.SECRET_CODE_LENGTH)

        for c in g.secret_code:
            self.assertIn(c, [c[0] for c in COLOR_CHOICES])

    def test_create_multiple_games(self):
        g1 = Game.objects.create(
            user=self.user
        )

        self.assertTrue(g1.active)

        g2 = Game.objects.create(
            user=self.user
        )

        self.assertTrue(g2.active)

        g1.refresh_from_db()

        # When creating a new game for a user the other game goes unactive

        self.assertFalse(g1.active)

        # If g1 is activated again, then g2 goes unactive
        g1.active = True
        g1.save()
        g1.refresh_from_db()
        g2.refresh_from_db()

        self.assertTrue(g1.active)
        self.assertFalse(g2.active)

        # If a game for another user gets created, g1 and g2 keep their state

        g3 = Game.objects.create(
            user=self.user2
        )

        g1.refresh_from_db()
        g2.refresh_from_db()

        self.assertTrue(g3.active)
        self.assertTrue(g1.active)
        self.assertFalse(g2.active)


@override_settings(SECRET_CODE_LENGTH=4)
class AttemptModelTest(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='test_user')
        self.game1 = Game.objects.create(
            user=self.user,
            secret_code=[BLUE, RED, GREEN, YELLOW]
        )

        self.game2 = Game.objects.create(
            user=self.user,
            secret_code=[BLUE, BLUE, YELLOW, BLACK]
        )

    def test_guess_result_correct(self):
        attempt = Attempt.objects.create(
            game=self.game1,
            guess=[WHITE, WHITE, WHITE, WHITE]
        )
        self.assertEqual(attempt.attempt_number, 1)
        self.assertEqual(attempt.result, [NONE, NONE, NONE, NONE])
        self.assertFalse(attempt.success)

        attempt2 = Attempt.objects.create(
            game=self.game1,
            guess=[BLUE, GREEN, RED, YELLOW]
        )
        self.assertEqual(attempt2.attempt_number, 2)
        self.assertEqual(attempt2.result, [BLACK, BLACK, WHITE, WHITE])
        self.assertFalse(attempt2.success)

        attempt3 = Attempt.objects.create(
            game=self.game1,
            guess=[RED, BLUE, YELLOW, GREEN]
        )
        self.assertEqual(attempt3.attempt_number, 3)
        self.assertEqual(attempt3.result, [WHITE, WHITE, WHITE, WHITE])
        self.assertFalse(attempt3.success)

        attempt4 = Attempt.objects.create(
            game=self.game1,
            guess=[BLUE, RED, GREEN, YELLOW]
        )
        self.assertEqual(attempt4.attempt_number, 4)
        self.assertEqual(attempt4.result, [BLACK, BLACK, BLACK, BLACK])
        self.assertTrue(attempt4.success)

        attempt12 = Attempt.objects.create(
            game=self.game2,
            guess=[BLACK, YELLOW, BLUE, BLUE]
        )
        self.assertEqual(attempt12.attempt_number, 1)
        self.assertEqual(attempt12.result, [WHITE, WHITE, WHITE, WHITE])
        self.assertFalse(attempt12.success)

    def test_game_finalization(self):
        Attempt.objects.create(
            game=self.game1,
            guess=[BLUE, RED, GREEN, YELLOW]
        )

        # If the attempt was correct, the game ends
        self.game1.refresh_from_db()
        self.assertFalse(self.game1.active)
        self.assertTrue(self.game1.completed)

        # Attempt MAX_ATTEMPTS to force game ending
        for i in range(settings.MAX_ATTEMPTS):
            Attempt.objects.create(
                game=self.game2,
                guess=[WHITE, WHITE, WHITE, WHITE]
            )

        # After attempting too many times, the game ends
        self.game2.refresh_from_db()
        self.assertFalse(self.game1.active)
        self.assertTrue(self.game1.completed)
