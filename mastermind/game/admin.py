# Register your models here.
from django.contrib import admin
from django.db.models import Count

from game.models import Game, Attempt


class AttemptAdminInline(admin.StackedInline):
    model = Attempt
    readonly_fields = ('attempt_number', 'result', 'success', 'created', 'modified')
    extra = 0


@admin.register(Game)
class GameAdmin(admin.ModelAdmin):
    list_display = ('id', 'username', 'active', 'completed', 'secret_code', 'attemptsnum')
    list_filter = ('active',)
    readonly_fields = ('secret_code', 'created', 'modified')
    model = Game
    inlines = [AttemptAdminInline]

    def get_queryset(self, request):
        queryset = super(GameAdmin, self).get_queryset(request)

        queryset = queryset.annotate(attempts_count=Count('attempts')).select_related('user')

        return queryset

    def attemptsnum(self, obj):
        return obj.attempts_count

    attemptsnum.short_description = 'Attempts #'

    def username(self, obj):
        return obj.user.username
