#!/usr/bin/env sh

set -u
set -e


# Wait for database
echo "Waiting for postgresql..."
until nc -z ${POSTGRES_HOST} ${POSTGRES_PORT}
do
  sleep 1
done
sleep 1

# Apply migrations
./manage.py migrate --noinput

# Generate static files
./manage.py collectstatic --noinput

if [ "${ENVIRON:-dev}" = "dev" ]
then
  ./manage.py runserver "0.0.0.0:8000"
else
  # If not dev use gunicorn
  gunicorn -w 1 --log-level info -b "0.0.0.0:8000" -t 60 mastermind.wsgi:application
fi
