[![pipeline status](https://gitlab.com/flapedriza/mastermind/badges/master/pipeline.svg)](https://gitlab.com/flapedriza/mastermind/commits/master)
[![coverage report](https://gitlab.com/flapedriza/mastermind/badges/master/coverage.svg)](https://gitlab.com/flapedriza/mastermind/commits/master)

# Mastermind API #

## Development environment ##
To prepare your development environment you will need [docker](https://docs.docker.com/install/) and
[docker-compose](https://docs.docker.com/compose/install/), after installing
them you can run the development server by simply executing the following commands from the path
where the `docker-compose.yml` file is located:
```bash
docker-compose build
docker-compose up
```
You can then run an interactive shell inside the backend container by executing
```bash
docker-compose exec backend sh
```

or a single command by executing
```bash
docker-compose exec backend <command>
# For example to enter shell_plus
docker-compose exec backend python manage.py shell_plus
```

## Before pushing ##
Before pushing your code it must pass all the tests and satisfy flake8 standards, otherwise the CI 
pipeline will fail. To check both conditions you can do it the following way:
```bash
# If docker-compose is up
docker-compose exec backend flake8
docker-compose exec backend python run_tests.py

# Otherwise
docker-compose run backend /bin/sh -c "flake8 && python run_tests.py"
```

## API description
This project provides a REST API to manage [mastermind](https://en.wikipedia.org/wiki/Mastermind_(board_game))
games.

To play a game, a user must be created in the system, and after registration an 
authentication token can be obtained by providing user and password.

All other actions require providing an authentication token through the `Authentication`
header and all actions will only affect games associated to the token user

## API documentation
Documentation for this API can be checked at `/api/redoc/` for a redoc-ui API documentation, 
`/api/swagger` for a swagger UI documentation or `/api/swagger.json` or `/api/swagger.yaml` for
either swagger json or yaml documentation 

